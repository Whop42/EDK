The EMC (Easy Minecraft Client) Development Kit
===================

Development kit for [EMC](https://gitlab.com/EMC-Framework/EMC)

Making your first mod with EMC
-------------------

1. You can simply clone this repo, import the `build.gradle` into your IDE of choice and start developing!

If you want more help you can check out this [example Discord rich presence mod](https://gitlab.com/EMC-Framework/EMC-Discord-RPC) made with EMC.

Installing your mod
-------------------

Once you're done with your mod, run the gradle `build` task. Copy your mod jar file from `build/libs/` to `.minecraft/libraries/EMC/<Minecraft version>/` and start Minecraft with EMC.

License
-------------------

EMC is licensed under GPL-3.0
